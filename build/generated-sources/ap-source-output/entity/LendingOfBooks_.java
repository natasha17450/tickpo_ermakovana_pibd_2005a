package entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LendingOfBooks.class)
public abstract class LendingOfBooks_ {

	public static volatile SingularAttribute<LendingOfBooks, Date> date;
	public static volatile SingularAttribute<LendingOfBooks, Date> dateReturn;
	public static volatile SingularAttribute<LendingOfBooks, String> remark;
	public static volatile SingularAttribute<LendingOfBooks, User> usersLogin;
	public static volatile SingularAttribute<LendingOfBooks, Integer> id;
	public static volatile SingularAttribute<LendingOfBooks, Books> booksId;

}

