package entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Books.class)
public abstract class Books_ {

	public static volatile SingularAttribute<Books, Date> date;
	public static volatile SingularAttribute<Books, String> author;
	public static volatile SingularAttribute<Books, Integer> id;
	public static volatile SingularAttribute<Books, String> text;
	public static volatile SingularAttribute<Books, String> title;
	public static volatile SingularAttribute<Books, Integer> releaseYear;

}

