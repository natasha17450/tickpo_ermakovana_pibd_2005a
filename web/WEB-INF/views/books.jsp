<%-- 
    Document   : index
    Created on : 06.03.2023
    Author     : Ermakova N.A.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <div id="main">
            <aside class="leftAside"> 
                <h2>Жанры</h2>
                <ul>
                    <li><a href="#">Фантастика</a></li>
                    <li><a href="#">Детектив</a></li>
                    <li><a href="#">Приключения</a></li>
                    <li><a href="#">Любовный роман</a></li>
                    <li><a href="#">Исторический роман</a></li>
                    <li><a href="#">Психология</a></li>
                    <li><a href="#">Детские книги</a></li>
                    <li><a href="#">Биография</a></li>
                    <li><a href="#">Триллер</a></li>
                    <li><a href="#">Бизнес</a></li>
                </ul>
            </aside>
            <section>
                <book>
                    <h1>Мастер и Маргарита</h1>
                    <div class="text-book">
                        Это вечная книга, прославившая Булгакова, которого не имеет определённого жанра. Здесь переплетаются фантастика, мистика, история, философия и есть место даже юмору. Более десяти лет писатель трудился над созданием знаменитого произведения, ставшим его завещанием.
                    </div>
                    <div class="fotter-book">
                        <span class="autor">Автор:<a href="#">Михаил Булгаков</a></span>
                        <span class="lending"><a href="javascript:void(0);">Выдать читателю</a></span>
                        <span class="date-book">Год: 2000</span>
                    </div>
                </book>
            </section>
        </div>
    </body>
</html>